OpenCV 4.5 with hash functions
================================

This repository contains OpenCV 4.5 with opencv_contrib 4.5 compiled for Android with Android wrappers. 
Available modules: core, java, imgproc, imgcodecs, img_hash (from contrib repo).

How was it built?
===============================

Prerequisites: Java 8, Python 2.7+, Python 3.6+, Java SDK (should be exists if Android Studio is installed), Java NDK (should be exists if Android Studio is installed).   

To build new OpenCV version or rebuild current version needs to download and unzip:

1. [OpenCV sources](https://opencv.org/releases/)

2. [OpenCV contrib sources](https://github.com/opencv/opencv_contrib). Select version according with OpenCV version

Next run build script (change folder names if needed):

        ./opencv-4.5.0/platforms/android/build_sdk.py \
            --extra_modules_path=./opencv_contrib-4.5.0/modules \
            --modules_list=img_hash,imgproc,imgcodecs,java \
            --use_android_buildtools \
            --no_samples_build \
           ./opencv_build 
           ./opencv-4.5.0
 

Troubleshooting
==================================================
If you have the next error during build project with OpenCV: 

        Path '/<path-to-project>/OpenCV/sdk/build/intermediates/compiled_local_resources/debug/out' is not a readable directory.

Just create this folder and rebuild the project 

